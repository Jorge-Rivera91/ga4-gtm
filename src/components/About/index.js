import ReactGA from "react-ga4";

const About = () => {
const handleClick = () => {
  ReactGA.event({
    category: "Button",
    action: "Click the button",
    label: "Click button with lable 'Click me!'", // optional
  });
}

  return (
    <>
      <span>About</span>
      <button onClick={handleClick} >Click me!</button>
    </>
  );
}

export default About;