
import './App.css';
import { useEffect } from 'react';
import ReactGA from "react-ga4";
import { BrowserRouter as Router, Switch, Route  } from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';


function App() {

  // useEffect(() => {
  //   ReactGA.initialize('G-55375C23Y8');

  //   ReactGA.send({ hitType: "pageview", page: `${window.location.pathname}${window.location.search}` });
  // }, []);


  return (
    <div className="App">
      <header className="App-header">
        Test Google Analytics
        <br />
        <a href='/home' >Home</a>
        <a href='/about' >About</a>
        <Router>
          <Switch>
            <Route path='/home'><Home /></Route>
            <Route path='/about'><About /></Route>
          </Switch>
        </Router>
      </header>
    </div>
  );
}

export default App;
